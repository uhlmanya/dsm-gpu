# Code by Yannick Uhlmann and Jannik Maier
# Calculate THD and SINAD depending on quantizier middle level and output middle level for 4-Level modulator 

#To Do's :
    # 3-level modulator rebuild like 4-Level and (add quantizier middle level array)
    # do simulation for different input signals and amplitudes
    # implement pwm modulator for comparison 



#%% 
from time import time
from matplotlib import pyplot as plt
import jax
import jax.numpy as jnp
from scipy import signal
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.ticker as mticker

jax.config.update('jax_platform_name', 'cpu')

#%%

def dsm(c_t, y_t):
    i_t_1, q_t_1 = c_t
    i_t          = i_t_1 + y_t - q_t_1
    q_t          = jnp.sign(i_t)
    return (i_t, q_t), q_t

def δσm_2(c_t, y_t):
    hi    = 0.25
    lo    = -hi
    i_t_1, q_t_1, ki, kp, fs, s_t_1 = c_t
    i_t   = i_t_1 + (ki * ((y_t - q_t_1/kp)/fs))
    q_msk = jnp.astype((i_t <= hi) & (i_t >= lo), float)
    his   = jnp.astype((i_t > hi), float)
    los   = -jnp.astype((i_t < lo), float)
    q_t   = q_msk * q_t_1 + his + los
    s_t   = jnp.astype(q_t_1 != q_t, float) + s_t_1
    return (i_t, q_t, ki, kp, fs, s_t), q_t

def δσm_3(c_t, y_t, hi, lo):
    hi    = 0.5
    mid   = 0.0
    lo    = -hi
    i_t_1, q_t_1, ki, kp, fs, s_t_1 = c_t
    i_t   = i_t_1 + (ki * ((y_t - q_t_1/kp)/fs))
    # q_msk = jnp.astype((i_t <= hi) & (i_t >= lo), float)
    # his   = jnp.astype((i_t > hi), float)
    # los   = -jnp.astype((i_t < lo), float)
    # q_t   = q_msk * q_t_1 + his + los
    q_t = schmitt_trigger_3(q_t_1, i_t, hi, mid, lo)
    s_t   = jnp.astype(q_t_1 != q_t, float) + s_t_1
    return (i_t, q_t, ki, kp, fs, s_t), q_t

def δσm_4(c_t, y_t):
    hi    = 0.75
    #mh    = 0.25
    #ml    = -mh
    lo    = -hi
    i_t_1, q_t_1, q_mh, out_mid_level, fs, s_t_1 = c_t
    kp    = jnp.ones(len(i_t_1))*1
    ki    = jnp.ones(len(i_t_1))*1180000
    
    i_t   = i_t_1 + (ki * ((y_t - q_t_1/kp)/fs))
    # q_msk = jnp.astype((i_t <= hi) & (i_t >= lo), float)
    # his   = jnp.astype((i_t > hi), float)
    # los   = -jnp.astype((i_t < lo), float)
    # q_t   = q_msk * q_t_1 + his + los
    q_t = schmitt_trigger_4(q_t_1, i_t, hi, q_mh, -q_mh, lo, out_mid_level)
    s_t   = jnp.astype(q_t_1 != q_t, float) + s_t_1
    return (i_t, q_t, q_mh, out_mid_level, fs, s_t), q_t #[q_t,q_t_1]

def schmitt_trigger_3(state,value, upper_threshold, middle_threshold, lower_threshold):      
    state_1 = jnp.where((state == 1.0)  & (value > middle_threshold),1.0,0.0)
    state_2 = jnp.where((state == 0.0)  & (value > upper_threshold),1.0,0.0)
    state_3 = jnp.where((state == 0.0)  & (value < lower_threshold),-1.0,0.0)
    state_4 = jnp.where((state == -1.0)  & (value < middle_threshold),-1.0,0.0) 
    state_neu = state_1+state_2+state_3+state_4    
    return state_neu

def schmitt_trigger_4(state,value, upper_threshold, middle_high, middle_low, lower_threshold, out_mid_level):      
    # state_1 = jnp.where(state == 1.0, jnp.where(value > middle_high, 1.0, out_mid_level), 0.0)   
    # state_2 = jnp.where(state == out_mid_level, jnp.where(value > upper_threshold, 1.0, out_mid_level), 0.0)
    # state_3 = jnp.where(state == out_mid_level, jnp.where(value < middle_low, -out_mid_level, out_mid_level), 0.0)
    # state_23 = jnp.where(state == out_mid_level, -out_mid_level,0) # korrektur um faktor da zwei mal addiert wird 
    # state_4 = jnp.where(state == -out_mid_level, jnp.where(value > middle_high, out_mid_level, -out_mid_level), 0.0)
    # state_5 = jnp.where(state == -out_mid_level, jnp.where(value < lower_threshold, -1.0, -out_mid_level), 0.0)
    # state_45 = jnp.where(state == -out_mid_level, out_mid_level,0) # korrektur um faktor da zwei mal addiert wird 
    # state_6 = jnp.where(state == -1.0, jnp.where(value > middle_low, -out_mid_level, -1.0), 0.0)
    # state_neu = state_1+state_2+state_3+state_4+state_5+state_6+state_23+state_45
    
    state_1 = ((state == 1.0)&(value > middle_high))*1.0 + ((state == 1.0)&(value <= middle_high))*out_mid_level
    state_2 = ((state == out_mid_level)&(value >= upper_threshold))*1.0 \
        + ((state == out_mid_level)&(value < upper_threshold)&(value > middle_low))*out_mid_level \
        + ((state == out_mid_level)&(value <= middle_low))*-out_mid_level
    state_3 = ((state == -out_mid_level)&(value >= middle_high))*out_mid_level \
        + ((state == -out_mid_level)&(value < middle_high)&(value > lower_threshold))*-out_mid_level \
        + ((state == -out_mid_level)&(value <= lower_threshold))*-1.0
    state_4 = ((state == -1.0)&(value >= middle_low))*-out_mid_level + ((state == -1.0)&(value < middle_low))*-1.0
    state_neu = state_1+state_2+state_3+state_4
    return state_neu


def lpf(s, fs = 1000000, fc = 1e6, o = 5):
    sos = signal.butter(o, fc, 'low', fs=fs, output='sos')
    return signal.sosfiltfilt(sos, s)

#%% inizialisieren
level = 4
ki = 1180000
kp = 1
n  = int(2 ** 25)
t  = jnp.linspace(0, 1, n)
fs = [100]#list(range(10,51,10))
amp = 0.4
now  = 5 # number of harmonics
#ys = jnp.array([amp*jnp.sin(2 * jnp.pi * f * t) for f in fs]).T
#ys = jnp.array(amp*jnp.sin(2 * jnp.pi * fs[0] * t)).T # sinus
ys = jnp.array((amp*1.15*(jnp.sin(2 * jnp.pi * fs[0] * t)+ jnp.sin(2 * jnp.pi * 3*fs[0] * t)/6))).T #third harmonic


#out_level_val = jnp.array([0.3,0.3,0.3,0.3,0.3,0.3,0.3,0.3])
out_level_val = jnp.linspace(0.1,0.9,7)
quantizion_mid_level_val = jnp.linspace(0.1,0.65,6)

out_level, quantizion_mid_level = jnp.meshgrid(out_level_val,quantizion_mid_level_val) # meashgrid

print(f"Maximum Rise Time: {1/n*1e9} ns")

#%% Modulator
tic   = time()
if level == 4:
    cs = ( jnp.zeros(len(out_level.reshape(-1,))), jnp.ones(len(out_level.reshape(-1,))) # )
         , quantizion_mid_level.reshape(-1,), out_level.reshape(-1,)
         , jnp.ones(len(out_level.reshape(-1,))) * n, jnp.zeros(len(out_level.reshape(-1,))))
    (_, _, _, _, _, ss), ms = jax.lax.scan(δσm_4, cs, ys)
    
elif level == 3:
    cs = ( jnp.zeros(len(fs)), jnp.zeros(len(fs)) # )
         , jnp.ones(len(fs)) * ki, jnp.ones(len(fs)) * kp
         , jnp.ones(len(fs)) * n, jnp.zeros(len(fs)))
    (_, _, _, _, _, ss), ms = jax.lax.scan(δσm_3, cs, ys)
    
else:
    cs = ( jnp.zeros(len(fs)), jnp.ones(len(fs))*1.0 # )
         , jnp.ones(len(fs)) * ki, jnp.ones(len(fs)) * kp
         , jnp.ones(len(fs)) * n, jnp.zeros(len(fs)))
    (_, _, _, _, _, ss), ms = jax.lax.scan(δσm_2, cs, ys)
    
ms = ms*400
sw_freq = ss/2
toc   = time()
sec   = toc - tic
print(f'Modulating {int(n * len(fs))} points took {sec:.3f}s')

#%% Filter
tic = time()
ls = jnp.stack([lpf(ms[:,i].squeeze(),fs=n,fc=2e6) for i in range(len(out_level.reshape(-1,)))]).T
toc   = time()
sec   = toc - tic
print(f'Filtering {int(n * len(fs))} points took {sec:.3f}s')

#%% FFT THD Sinus

tic = time()
win = jnp.array(signal.windows.hann(n)).reshape(-1,1)
mag = jnp.abs(jax.vmap(jnp.fft.fft, in_axes=1,out_axes=1)(ls * win))
fw  = jnp.fft.fftfreq(n, 1/n)
nrm = mag[fw >= 0,:] / mag[jnp.stack(fw == fs[0])]
nyq = 20 * jnp.log10(nrm)
toc   = time()
sec   = toc - tic
#print(f'FFT {int(n * len(fs))} points took {sec:.3f}s')


win_in = jnp.array(signal.windows.hann(n))
inf = jnp.abs(jnp.fft.fft(ys * win_in))
inn = jnp.array(inf[fw >= 0] / inf[jnp.stack(fw == fs[0])]).reshape(-1,1)


harm = jnp.stack([ (nrm - inn)[fs[0] * jnp.arange(2,now+2,1),i]for i in range(ms.shape[1]) ]).T.sum(axis=0)
#harm = jnp.stack([ (nrm - inn)[fs[0] * jnp.arange(2,now+2,1)]]).T.sum(axis=0)
thd = 20 * jnp.log10(1/harm)

#ampl = jnp.stack([(nrm - inn)[ jnp.arange(fs[0]/10,fs[0]*10,dtype=int)].squeeze().sum()])
ampl = jnp.sum((nrm - inn)[int(fs[0]/10):int(fs[0]*10), :],axis=0)
sinad = 20 * jnp.log10(1/ampl)

#print(f'THD = {thd}, SINAD = {sinad}')

#%% Plot

# #y = ys[:,2].squeeze()
# m = ms[:,2].squeeze()
# l = ls[:,2].squeeze()


# plt.figure(1)
# # plt.plot(t, ys, label='Signal')
# # plt.plot(t, m.squeeze(), label='Modulator')
# # plt.plot(t, l, label='Filtered')
# #plt.plot(t, ms[0].squeeze(), label='Modulator')
# #plt.plot(t, ms[1].squeeze(), label='Integrator')
# #plt.plot(t, ms[2].squeeze(), label='state_1')
# for col in range(ms.shape[1]):
#     plt.subplot(ms.shape[1], 1,col+1)
#     plt.plot(ms[:, col][int(0):int(0.01*n)], label=f'Spalte {col+1}')

# plt.xlabel('Time in s')
# plt.ylabel('Value')
# plt.legend(loc = 'upper left')
# plt.grid()
# plt.show()

# plt.figure(2)
# #_ = [ plt.plot(nyq[:,i].squeeze(), alpha=0.5, label=f'{f}Hz')
# #      for i,f in enumerate(fs) ]
# for col in range(nyq.shape[1]):
#     plt.plot(nyq[:, col], label=f'Spalte {col+1}')
# plt.xlabel('Frequency in Hz')
# plt.ylabel('Magnitude in dB')
# plt.legend(loc='upper left')
# plt.xscale('log')
# plt.show()

#%% results

# plt.figure(3)
# plt.subplot(2,1,1)
# # plt.plot(out_level*400,thd, label = 'THD')
# # plt.plot(out_level*400,sinad, label = 'Sinad')
# plt.plot(quantizion_mid_level,thd, label = 'THD')
# plt.plot(quantizion_mid_level,sinad, label = 'Sinad')
# plt.xlabel(u'\u00B1 middle Level')
# plt.ylabel('THD/SINAD in dB')

# plt.subplot(2,1,2)
# # plt.plot(out_level*400,sw_freq/1000)
# plt.plot(quantizion_mid_level,sw_freq/1000)
# plt.xlabel(u'\u00B1 middle Level')
# plt.ylabel('Switching frequenz in kHz')

# plt.tight_layout()
# plt.show()

#%% 3D

td = plt.figure(4)
ax = td.add_subplot(111,projection='3d')
ax.plot_surface(out_level, quantizion_mid_level,thd.reshape(out_level.shape))
ax.set_xlabel(u'\u00B1 middle Level')
ax.set_ylabel('Quantizier middle level')
ax.set_zlabel('THD in dB')
ax.set_title('THD')

td = plt.figure(5)
ax = td.add_subplot(111,projection='3d')
ax.plot_surface(out_level, quantizion_mid_level,sinad.reshape(out_level.shape))
ax.set_xlabel(u'\u00B1 middle Level')
ax.set_ylabel('Quantizier middle level')
ax.set_zlabel('SINAD in dB')
ax.set_title('SINAD')

td = plt.figure(6)
ax = td.add_subplot(111,projection='3d')
# plt.plot(out_level*400,thd, label = 'THD')
# plt.plot(out_level*400,sinad, label = 'Sinad')
ax.plot_surface(out_level, quantizion_mid_level,sw_freq.reshape(out_level.shape)/1000)
#ax.zaxis.set_major_formatter(mticker.ScalarFormatter(useMathText=False))
#plt.plot(quantizion_mid_level,sinad, label = 'Sinad')
ax.set_xlabel(u'\u00B1 middle Level')
ax.set_ylabel('Quantizier middle level')
ax.set_zlabel('sw in kHz')
ax.set_title('Switching Frequency')
from time import time
from matplotlib import pyplot as plt
import jax
import jax.numpy as jnp
from scipy import signal
import pandas as pd

# jax.config.update('jax_platform_name', 'cpu')
jax.config.update('jax_default_device', jax.devices()[1])

def dsm(c_t, y_t):
    i_t_1, q_t_1 = c_t
    i_t          = i_t_1 + y_t - q_t_1
    q_t          = jnp.sign(i_t)
    return (i_t, q_t), q_t

def δσm(c_t, y_t):
    hi    = 0.5
    lo    = -hi
    i_t_1, q_t_1, ki, kp, fs, s_t_1 = c_t
    i_t   = i_t_1 + (ki * ((y_t - q_t_1/kp)/fs))
    q_msk = jnp.astype((i_t <= hi) & (i_t >= lo), float)
    his   = jnp.astype((i_t > hi), float)
    los   = -jnp.astype((i_t < lo), float)
    q_t   = q_msk * q_t_1 + his + los
    s_t   = jnp.astype(q_t_1 != q_t, float) + s_t_1
    return (i_t, q_t, ki, kp, fs, s_t), q_t

def lpf(s, fs = 1000000, fc = 1e6, o = 5):
    sos = signal.butter(o, fc, 'low', fs=fs, output='sos')
    return signal.sosfiltfilt(sos, s)

ki = 1180000
kp = 1
n  = int(2 ** 13)
t  = jnp.linspace(0, 1, n)
fs = list(range(50,501,20))
ys = jnp.array([jnp.sin(2 * jnp.pi * f * t) for f in fs]).T
cs = ( jnp.zeros(len(fs)), -jnp.ones(len(fs)) )
     # , jnp.ones(len(fs)) * ki, jnp.ones(len(fs)) * kp
     # , jnp.ones(len(fs)) * n, jnp.zeros(len(fs)))

tic   = time()
_, ms = jax.lax.scan(dsm, cs, ys)
toc   = time()
sec   = toc - tic
print(f'Modulating {int(n * len(fs))} points took {sec:.3f}s')

ls = jnp.stack([lpf(ms[:,i].squeeze(),fs=n) for i in range(len(fs))]).T

y = ys[:100,2].squeeze()
m = ms[:100,2].squeeze()
#l = ls[:,2].squeeze()

plt.plot(t[:100], y, label='Signal')
plt.plot(t[:100], m, label='Modulated')
#plt.plot(t, l, label='Filtered')
plt.xlabel('Time in s')
plt.ylabel('Value')
plt.legend(loc = 'upper left')
plt.show()

df = pd.DataFrame(jnp.vstack([t[:100], ys[:100,2], ms[:100,2]]).T, columns=['t','y','m'])
df.to_csv('./signal.csv', index = False)

win = jnp.array(signal.windows.hann(n)).reshape(-1,1)
mag = jnp.abs(jax.vmap(jnp.fft.fft, in_axes=1,out_axes=1)(ls * win))
fw  = jnp.fft.fftfreq(n, 1/n)
nrm = mag[fw >= 0,:] / mag[jnp.stack([fw == f for f in fs]).T]
nyq = 20 * jnp.log10(nrm + 1e-20)

_ = [ plt.plot(nyq[:,i].squeeze(), alpha=0.5, label=f'{f}Hz')
      for i,f in enumerate(fs) ]
plt.xlabel('Frequency in Hz')
plt.ylabel('Magnitude in dB')
plt.legend(loc='upper left')
plt.xscale('log')
plt.show()

inf = jnp.abs(jax.vmap(jnp.fft.fft, in_axes=1,out_axes=1)(ys * win))
inn = inf[fw >= 0,:] / mag[jnp.stack([fw == f for f in fs]).T]

now  = 3
harm = jnp.stack([ (nrm - inn)[f * jnp.arange(2,now+2,1),i]
                   for i,f in enumerate(fs) ]).T.sum(axis=0)
thd = 20 * jnp.log10(1/harm)

ampl = jnp.stack([(nrm - inn)[ jnp.arange(f/10,f*10,dtype=int),i
                             ].squeeze().sum() for i,f in enumerate(fs) ])
sinad = 20 * jnp.log10(1/ampl)

print(f'THD = {thd}, SINAD = {sinad}')

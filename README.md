# ∆ΣM on GPU 

Vectorized ∆Σ-Modulator in JAX on GPU in `noloop-jax.py`. PyTorch can't compete
without `scan`, so **don't use** `noloop-pt.py` as it is broken.

## Performance

Tested with $n = 2^{24}\times 10 = 167772160$ points, takes $\approx 130$
seconds.

## Known Issues

Warning about misaligned CUDA driver is expected. This will be resolved after
PLASMA server udpate. Could speed up computation even further.

from time import time
from matplotlib import pyplot as plt
import torch as pt

if pt.cuda.is_available():
    pt.set_default_device('cuda')
    pt.cuda.set_device('cuda:1')

n = int(2 ** 20)
f = 100
t = pt.linspace(0, 2, n)
y = pt.sin(2 * pt.pi * f * t)
c = (0.0,0.0)

def dsm(y_t):
    global c
    i_t_1, q_t_1 = c
    i_t          = i_t_1 + y_t - q_t_1
    q_t          = pt.sign(i_t)
    c            = (i_t, q_t)
    return q_t

m_t = pt.tensor(list(map(dsm,list(y.cpu()))))

tic  = time()
m    = pt.vmap(dsm)(y)
toc  = time()
sec  = toc - tic
print(f'Modulating {n} points took {sec:.3f}s')

plt.plot(t.cpu(),y.cpu())
plt.plot(t.cpu(),m.cpu())
plt.show()
